var myDropdowns = [];

// NOTE Initializes all elements with the class "dropdown" into myDropdown
//      Params:
//        (none)
//
function initMyDropdowns() {
  var allDropdowns = document.querySelectorAll(".dropdown");

  for (var i = 0; i < allDropdowns.length; i++) {
    myDropdowns.push(new MyDropdown(allDropdowns[i]));
  }
}

// NOTE Returns a MyDropdown initialized object with the given id
//      Params:
//        @elementId: the DOM id of the dropdown to look for
//
function getMyDropdown(elementId) {
  for (var i = 0; i < myDropdowns.length; i++) {
    if ( myDropdowns[i].el.id == elementId ) {
      return myDropdowns[i];
    }
  }

  return undefined;
}

// NOTE The constructor of the MyDropdown prototype.
//      Params:
//        @el - the element with the "dropdown" class.
//
function MyDropdown(el) {
  this.el = el;

  if ( !this.el.id ) {
    this.el.id = "my-dropdown-id-" + myDropdowns.length;
  }

  this.toggle = this.el.querySelector(".toggle");
  this.submenu = this.el.querySelector(".submenu");
  this.items = this.submenu.querySelectorAll(".item");

  this.init();

  return this;
}

// NOTE Called by the constructor, it initializes the myDropdown element, and
//      adds some event listeners on it.
//
MyDropdown.prototype.init = function() {
  if ( this.el ) {
    this.toggle.addEventListener("click", this);

    // NOTE automatic means that clicking an item should close the dropdown and
    //      select it. This is not needed when the items are external urls.
    if ( this.el.classList.contains("automatic") ) {
      for (var i = 0; i < this.items.length; i++) {
        this.items[i].addEventListener("click", this);
      }
    }

    // NOTE For short lists, don't break columns.
    //      (.items should be float: left)
    if ( this.items.length < 10 ) {
      for (var i = 0; i < this.items.length; i++) {
        this.items[i].classList.add("no-float");
      }
    }
  }
}

// NOTE Returns the selected value of the dropdown.
//
MyDropdown.prototype.value = function() {
  return this.el.dataset.value;
}

// NOTE Returns the humanized version of the selected value - see comment for
//      humanizeItemValue for more details.
//
MyDropdown.prototype.humanizeSelected = function() {
  return this.humanizeItemValue(this.value());
}

// NOTE Returns the humanized version of the given value - that is what the
//      user sees when selecting the dropdown. The return string can have html since
//      the dropdown can have images, links etc for each selection.
//      Params:
//        @value - the data value of the dropdown item to humanize.
//
//      Example:
//        myDropdown.humanizeItemValue("1");
//        > "RED"
//
MyDropdown.prototype.humanizeItemValue = function(value) {
  if ( value ) {
    var item = this.el.querySelector(".item[data-value='" + value + "']");

    if ( item ) {
      return item.innerHTML;
    }
  }

  return undefined;
}

MyDropdown.prototype.handleEvent = function (e) {
  switch(e.type) {
    case "click": {
      if ( e.currentTarget === this.toggle ) {
        this.clickToggleEvent(e);
      } else if ( e.currentTarget === document ) {
        this.closeDropdownsEvent(e);
      } else {
        // NOTE We assume all other cases are clicked items, to avoid iterating
        // over all items to check the currentTarget.
        this.clickItemEvent(e);
      }
    }
  }
}

MyDropdown.prototype.positionPopup = function() {
  newTop = this.toggle.getBoundingClientRect().top + this.toggle.offsetHeight + window.pageYOffset;
  newLeft = this.toggle.getBoundingClientRect().left + window.pageXOffset;
  minWidth = this.toggle.offsetWidth - (this.submenu.offsetWidth - this.submenu.width);

  this.submenu.style.cssText = "left: " + newLeft + "px; top: " + newTop + "px; min-width: " + minWidth + "px";
}

MyDropdown.prototype.clickToggleEvent = function(e) {
  if ( !this.el.classList.contains("open") ) {
    // NOTE Hide all open dropdowns
    for (var i = 0; i < myDropdowns.length; i++) {
      myDropdowns[i].el.classList.remove("open");
    }

    this.positionPopup();

    this.el.classList.add("open");

    // NOTE Make sure document doesn't have a myDropdown event listener before
    //      adding it
    document.removeEventListener("click", this);
    document.addEventListener("click", this);
  } else {
    this.el.classList.remove("open");
  }
}

MyDropdown.prototype.closeDropdownsEvent = function(e) {
  if ( this.el !== e.target && !this.el.contains(e.target) ) {
    // NOTE Remove the event so that document doesn't keep listening
    document.removeEventListener("click", this);

    this.el.classList.remove("open");
  }
}

MyDropdown.prototype.clickItemEvent = function(e) {
  var clickedItem = e.currentTarget;

  this.el.classList.remove("open");
  for (var i = 0; i < this.items.length; i++) {
    this.items[i].classList.remove("selected");
  }

  this.toggle.innerHTML = clickedItem.textContent;

  // NOTE Transfer selected data to .dropdown to be retrievable by scripts
  if ( clickedItem.dataset.value ) {
    this.el.dataset.value = clickedItem.dataset.value;
  } else {
    delete this.el.dataset.value;
  }

  clickedItem.classList.add("selected");
  this.el.dispatchEvent(new Event("change"));
}

// TODO Remove from myDropdowns too
//
MyDropdown.prototype.destroy = function() {
  this.el.classList.remove("open");

  document.removeEventListener("click", this);
  this.toggle.removeEventListener("click", this);

  for (var i = 0; i < this.items.length; i++) {
    this.items[i].removeEventListener("click", this);
  }
}
