document.addEventListener("DOMContentLoaded", function() {
  initMyDropdowns();

  myDropdowns[2].el.addEventListener("change", function() {
    document.querySelector("#title").innerHTML = "";

    if ( this.dataset.value ) {
      document.querySelector("#title").innerHTML = this.dataset.value;
    }
  });
});

